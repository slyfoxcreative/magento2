# magento2

Magento 2 API library.

```php
use SlyFoxCreative\Magento2\Client;

$client = Client::make('https://example.com', 'token');

$product = $client->products()->find('TST*TEST01');
echo "{$product->sku}\n";
echo "{$product->updatedAt}\n";
```
