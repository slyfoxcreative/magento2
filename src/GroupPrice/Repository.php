<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\GroupPrice;

use Illuminate\Support\Collection;
use SlyFoxCreative\Magento2\Client;
use SlyFoxCreative\Magento2\Exception\NotFoundException;
use SlyFoxCreative\Magento2\Repository as Base;

/**
 * A repository of group prices.
 */
class Repository extends Base
{
    public function __construct(
        Client $client,
        private readonly string $sku,
        private readonly int $groupId,
    ) {
        parent::__construct($client, GroupPrice::class);
    }

    public function find(int|string $key): GroupPrice
    {
        if (! is_int($key)) {
            throw new \TypeError('Invalid key type for GroupPrice');
        }

        return new GroupPrice($key, $this);
    }

    /**
     * Get the raw collection of data for the given ID.
     */
    public function data(int|string $key): Collection
    {
        $key = strval($key);
        if (! $this->cache->has($key)) {
            $this->all();
        }

        if (! $this->cache->has($key)) {
            throw new NotFoundException(
                "Product {$this->sku} doesn't have a group price for group {$this->groupId} and quantity {$key}.",
            );
        }

        return $this->cache[$key];
    }

    /**
     * Delete the group price for the given SKU, customer group ID and quantity.
     */
    public function delete(int $quantity): bool
    {
        $basePath = $this->basePath();
        $result = $this->client->delete("/{$basePath}/{$quantity}");
        $this->cache->forget(strval($quantity));

        return $result;
    }

    protected function resources(): Collection
    {
        $basePath = $this->basePath();

        return $this->client->get("/{$basePath}");
    }

    /**
     * Return the base API path for the Repository.
     */
    protected function basePath(): string
    {
        return "products/{$this->sku}/group-prices/{$this->groupId}/tiers";
    }

    /**
     * Return the attribute to use for as they key when storing a Resource
     * in the Repository cache.
     */
    protected function cacheKey(): string
    {
        return 'qty';
    }
}
