<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\GroupPrice;

use SlyFoxCreative\Magento2\Resource;

/**
 * A customer group API resource.
 */
class GroupPrice extends Resource
{
    public function __construct(int $quantity, Repository $repository)
    {
        parent::__construct($quantity, $repository);
    }
}
