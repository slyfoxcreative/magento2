<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Exception;

/**
 * Thrown when an API call returns a 400 status code.
 */
class BadRequestException extends RequestException {}
