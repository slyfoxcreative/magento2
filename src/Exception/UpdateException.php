<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Exception;

/**
 * Thrown when attempting to get an Update a Resource that doesn't
 * support updates.
 */
class UpdateException extends \Exception
{
    public function __construct(string $class)
    {
        $class = class_basename($class);
        parent::__construct("{$class} does not support updates");
    }
}
