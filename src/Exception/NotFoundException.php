<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Exception;

/**
 * Thrown when an API call returns a 404 status code.
 */
class NotFoundException extends RequestException {}
