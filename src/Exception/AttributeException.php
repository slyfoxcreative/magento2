<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Exception;

/**
 * Thrown when attempting to access a nonexistent attribute on a Resource.
 */
class AttributeException extends \Exception
{
    public function __construct(string $class, string $name)
    {
        $class = class_basename($class);
        parent::__construct("No attribute {$name} for {$class}");
    }
}
