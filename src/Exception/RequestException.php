<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Exception;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Uri;

/**
 * Thrown when an API call returns an error status code.
 */
class RequestException extends \Exception
{
    final public function __construct(
        string $message,
        private readonly ?string $uri = null,
        private readonly ?string $body = null,
    ) {
        parent::__construct($message);
    }

    public static function fromGuzzle(ClientException $exception): self
    {
        $uri = (string) $exception->getRequest()->getUri();
        $body = (string) $exception->getResponse()->getBody();

        $data = json_decode($body, true);

        if (isset($data['parameters'])) {
            /** @var array<string, mixed> */
            $parameters = $data['parameters'];
            $parameters = collect($parameters);
            $keys = $parameters
                ->keys()
                ->map(function ($key) {
                    return "%{$key}";
                })
                ->all()
            ;
            $values = $parameters->values()->all();
            $message = str_replace($keys, $values, $data['message']);
        } elseif (isset($data['message'])) {
            $message = $data['message'];
        } else {
            $message = $exception->getMessage();
        }

        return new static($message, $uri, $body);
    }

    /**
     * Get the request URI.
     */
    public function uri(): ?string
    {
        return $this->uri;
    }

    /**
     * Get the response body.
     */
    public function body(): ?string
    {
        return $this->body;
    }
}
