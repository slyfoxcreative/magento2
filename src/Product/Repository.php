<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Product;

use SlyFoxCreative\Magento2\Client;
use SlyFoxCreative\Magento2\Repository as Base;

/**
 * A repository of Products.
 */
class Repository extends Base
{
    public function __construct(Client $client)
    {
        parent::__construct($client, Product::class);
    }

    public function find(int|string $key): Product
    {
        if (! is_string($key)) {
            throw new \TypeError('Invalid key type for Product');
        }

        return new Product($key, $this);
    }

    public function update(int|string $key): Update
    {
        return new Update($key, $this);
    }

    /**
     * {@inheritdoc}
     */
    protected function cacheKey(): string
    {
        return 'sku';
    }
}
