<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Product;

use Carbon\Carbon;
use SlyFoxCreative\Magento2\Update as Base;

/**
 * Encapsulates data for an update to a product.
 */
class Update extends Base
{
    /**
     * Add a price to the update data.
     */
    public function price(float $price): self
    {
        $this->request['price'] = $price;

        return $this;
    }

    /**
     * Add a group discount to the update data.
     */
    public function groupDiscount(int $groupId, float $discount): self
    {
        $this->request['tier_prices'][] = [
            'customer_group_id' => $groupId,
            'qty' => 1,
            'extension_attributes' => [
                'percentage_value' => $discount,
            ],
        ];

        return $this;
    }

    public function specialPrice(float $price, Carbon $startDate, Carbon $endDate): self
    {
        $this->request['custom_attributes'][] = [
            'attribute_code' => 'special_price',
            'value' => $price,
        ];
        $this->request['custom_attributes'][] = [
            'attribute_code' => 'special_from_date',
            'value' => $startDate->format('Y-m-d'),
        ];
        $this->request['custom_attributes'][] = [
            'attribute_code' => 'special_to_date',
            'value' => $endDate->format('Y-m-d'),
        ];

        return $this;
    }

    /**
     * Add a stock quantity to the update data.
     */
    public function quantity(int $quantity): self
    {
        $this->request['extension_attributes']['stock_item'] = [
            'qty' => $quantity,
            'is_in_stock' => $quantity > 0,
        ];

        return $this;
    }

    /**
     * Add a GTIN to the update data.
     */
    public function gtin(string $gtin): self
    {
        $this->request['custom_attributes'][] = [
            'attribute_code' => 'gtin',
            'value' => $gtin,
        ];

        return $this;
    }
}
