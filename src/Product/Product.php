<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Product;

use Carbon\Carbon;
use Illuminate\Support\Str;
use SlyFoxCreative\Magento2\Resource;

/**
 * A product API resource.
 */
class Product extends Resource
{
    public function __construct(string $sku, Repository $repository)
    {
        parent::__construct($sku, $repository);
    }

    /**
     * {@inheritdoc}
     *
     * Any attributes included in the Product's custom_attributes list can be
     * accessed directly as attributes.
     */
    public function __get(string $name): mixed
    {
        if ($this->hasAttributeMethod($name)) {
            return $this->callAttributeMethod($name);
        }

        $name = Str::snake($name);
        if ($this->data('customAttributes')->pluck('attribute_code')->contains($name)) {
            return $this
                ->data('customAttributes')
                ->where('attribute_code', $name)
                ->first()
                ->get('value')
            ;
        }

        return $this->data($name);
    }

    /**
     * {@inheritdoc}
     */
    public function __isset(string $name): bool
    {
        return $this->data('customAttributes')->pluck('attribute_code')->contains(Str::snake($name))
            || parent::__isset($name);
    }

    /**
     * Return an Update for this Product.
     */
    public function update(): Update
    {
        return new Update($this->key, $this->repository);
    }

    public function discountFor(int $groupId): float
    {
        $discount = $this->data('tier_prices')
            ->where('customer_group_id', $groupId)
            ->where('qty', 1)
            ->first()
        ;
        if (is_null($discount)) {
            return 0;
        }

        return $discount->get('extension_attributes')->get('percentage_value');
    }

    /**
     * Return the product's stock quantity.
     */
    protected function attributeQuantity(): int
    {
        return $this->extensionAttributes['stock_item']['qty'];
    }

    /**
     * Return the product's in-stock status.
     */
    protected function attributeIsInStock(): bool
    {
        return $this->extensionAttributes['stock_item']['is_in_stock'];
    }

    protected function attributeSpecialPrice(): float
    {
        $value = $this->data('customAttributes')->where('attribute_code', 'special_price')->first()->get('value');

        return floatval($value);
    }

    protected function attributeSpecialFromDate(): Carbon
    {
        $value = $this->data('customAttributes')->where('attribute_code', 'special_from_date')->first()->get('value');

        return Carbon::parse($value);
    }

    protected function attributeSpecialToDate(): Carbon
    {
        $value = $this->data('customAttributes')->where('attribute_code', 'special_to_date')->first()->get('value');

        return Carbon::parse($value);
    }
}
