<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2;

use Illuminate\Support\Collection;

/**
 * Recursively convert an array into a Collection.
 *
 * @param  array<int|string, mixed>  $array
 * @return Collection<int|string, mixed>
 */
function recursiveCollect(array $array): Collection
{
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $value = recursiveCollect($value);
            $array[$key] = $value;
        }
    }

    return collect($array);
}
