<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\CustomerGroup;

use SlyFoxCreative\Magento2\Resource;

/**
 * A customer group API resource.
 */
class CustomerGroup extends Resource
{
    public function __construct(int $id, Repository $repository)
    {
        parent::__construct($id, $repository);
    }
}
