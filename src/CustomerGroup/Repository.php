<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\CustomerGroup;

use SlyFoxCreative\Magento2\Client;
use SlyFoxCreative\Magento2\Repository as Base;

/**
 * A repository of CustomerGroups.
 */
class Repository extends Base
{
    public function __construct(Client $client)
    {
        parent::__construct($client, CustomerGroup::class);
    }

    public function find(int|string $key): CustomerGroup
    {
        if (! is_int($key)) {
            throw new \TypeError('Invalid key type for CustomerGroup');
        }

        return new CustomerGroup($key, $this);
    }

    /**
     * {@inheritdoc}
     */
    protected function allPath(): string
    {
        return parent::allPath() . '/search';
    }

    /**
     * {@inheritdoc}
     */
    protected function cacheKey(): string
    {
        return 'id';
    }
}
