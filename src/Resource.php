<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2;

use Carbon\Carbon;
use Illuminate\Support\Str;
use SlyFoxCreative\Magento2\Exception\AttributeException;

use function SlyFoxCreative\Utilities\assert_callable;

/**
 * An API resource such as a product or category.
 */
abstract class Resource
{
    public function __construct(
        protected readonly int|string $key,
        protected readonly Repository $repository,
    ) {
        $this->data();
    }

    /**
     * Get the attribute value for the given name.
     *
     * In addition to the data returned by the API, Resources can define
     * attribute methods to provide additional data or mutate existing data.
     * For example, the attributeCreatedAt() method returns the Resource's
     * created_at attribute converted to a Carbon object.
     */
    public function __get(string $name): mixed
    {
        if (! isset($this->{$name})) {
            throw new AttributeException(get_class($this), $name);
        }

        if ($this->hasAttributeMethod($name)) {
            return $this->callAttributeMethod($name);
        }

        return $this->data($name);
    }

    /**
     * Query whether an attribute with the given name exists.
     */
    public function __isset(string $name): bool
    {
        return $this->hasAttributeMethod($name) || $this->hasData($name);
    }

    /**
     * Dump an array of all data returned by the API.
     *
     * @return array<string, mixed>
     */
    public function dump(): array
    {
        return $this->data()->toArray();
    }

    /**
     * Return a value from the Resource data.
     *
     * If no argument is passed, return the entire data collection.
     */
    protected function data(?string $name = null): mixed
    {
        $data = $this->repository->data($this->key);

        if (is_null($name)) {
            return $data;
        }

        return $data[Str::snake($name)];
    }

    /**
     * Query whether the resource defines an attribute method for a given
     * name.
     */
    protected function hasAttributeMethod(string $name): bool
    {
        $method = 'attribute' . Str::studly($name);

        return method_exists($this, $method);
    }

    /**
     * Call the attributeMethod for the given name.
     */
    protected function callAttributeMethod(string $name): mixed
    {
        $method = 'attribute' . Str::studly($name);

        $callable = [$this, $method];
        assert_callable($callable, allowNonPublic: true);
        return call_user_func($callable);
    }

    /**
     * Get the creation datetime for the Resource.
     */
    protected function attributeCreatedAt(): Carbon
    {
        return Carbon::parse($this->data('createdAt'));
    }

    /**
     * Get the last-update datetime for the Resource.
     */
    protected function attributeUpdatedAt(): Carbon
    {
        return Carbon::parse($this->data('updatedAt'));
    }

    /**
     * Query whether the Resource has data for a given name.
     */
    private function hasData(string $name): bool
    {
        return $this->data()->has(Str::snake($name));
    }
}
