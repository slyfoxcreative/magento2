<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Collection;
use Psr\Log\LoggerInterface;
use SlyFoxCreative\Magento2\CustomerGroup\Repository as CustomerGroupRepository;
use SlyFoxCreative\Magento2\Exception\BadRequestException;
use SlyFoxCreative\Magento2\Exception\NotAuthorizedException;
use SlyFoxCreative\Magento2\Exception\NotFoundException;
use SlyFoxCreative\Magento2\GroupPrice\Repository as GroupPriceRepository;
use SlyFoxCreative\Magento2\Product\Repository as ProductRepository;

/**
 * A client for the Magento 2 REST API.
 */
class Client
{
    private readonly string $basePath;

    private readonly CustomerGroupRepository $customerGroups;

    private readonly ProductRepository $products;

    /** @var Collection<string, GroupPriceRepository> */
    private readonly Collection $groupPrices;

    public function __construct(
        private readonly GuzzleClient $client,
        private readonly string $token,
        string $code = 'default',
        private readonly bool $dump = false,
        private readonly ?LoggerInterface $logger = null,
    ) {
        $this->basePath = "/rest/{$code}/V1";
        $this->customerGroups = new CustomerGroupRepository($this);
        $this->products = new ProductRepository($this);
        $this->groupPrices = collect();
    }

    /**
     * Make a new Client object for the given site and username.
     *
     * This method takes care of authenticating the user and obtaining an
     * authentication token.
     */
    public static function make(
        string $uri,
        string $token,
        string $code = 'default',
        bool $dump = false,
        ?LoggerInterface $logger = null,
    ): self {
        $client = new GuzzleClient(['base_uri' => $uri]);

        return new self($client, $token, $code, $dump, $logger);
    }

    /**
     * Get the CustomerGroup Repository for this client.
     */
    public function customerGroups(): CustomerGroupRepository
    {
        return $this->customerGroups;
    }

    /**
     * Get the Product Repository for this client.
     */
    public function products(): ProductRepository
    {
        return $this->products;
    }

    /**
     * Get the GroupPrice Repository for this client and the given SKU
     * and customer-group ID.
     */
    public function groupPrices(string $sku, int $groupId): GroupPriceRepository
    {
        if (! isset($this->groupPrices["{$sku}/{$groupId}"])) {
            $this->groupPrices->put("{$sku}/{$groupId}", new GroupPriceRepository($this, $sku, $groupId));
        }

        return $this->groupPrices["{$sku}/{$groupId}"];
    }

    /**
     * Send a GET request to an API endpoint.
     *
     * @param  array<string, mixed>  $query
     * @return Collection<int|string, mixed>
     */
    public function get(string $path, array $query = []): Collection
    {
        return $this->request('GET', $path, $query);
    }

    /**
     * Send a POST request to an API endpoint.
     *
     * @param  array<string, mixed>  $data
     * @return Collection<int|string, mixed>
     */
    public function post(string $path, array $data = []): Collection
    {
        return $this->request('POST', $path, $data);
    }

    /**
     * Send a PUT request to an API endpoint.
     *
     * @param  array<string, mixed>  $data
     * @return Collection<int|string, mixed>
     */
    public function put(string $path, array $data = []): Collection
    {
        return $this->request('PUT', $path, $data);
    }

    /**
     * Send a DELETE request to an API endpoint.
     */
    public function delete(string $path): bool
    {
        return $this->request('DELETE', $path)['result'];
    }

    /**
     * Send a request to an API endpoint.
     *
     * @param  null|array<string, mixed>  $data
     * @return Collection<int|string, mixed>
     */
    private function request(string $method, string $path, ?array $data = null): Collection
    {
        $options = [
            'headers' => [
                'Authorization' => "Bearer {$this->token}",
                'Accept' => 'application/json',
            ],
        ];
        if (! is_null($data)) {
            $options[$method === 'GET' ? 'query' : 'json'] = $data;
        }

        $path = ltrim($path, '/');
        $path = "{$this->basePath}/{$path}";

        try {
            $response = $this->client->request($method, $path, $options);
        } catch (ClientException $exception) {
            switch ($exception->getResponse()->getStatusCode()) {
                case 400:
                    throw BadRequestException::fromGuzzle($exception);
                case 401:
                    throw NotAuthorizedException::fromGuzzle($exception);
                case 404:
                    throw NotFoundException::fromGuzzle($exception);
                default:
                    throw $exception;
            }
        }

        if ($this->dump) {
            echo $response->getBody();
        }

        $body = (string) $response->getBody();

        if (isset($this->logger)) {
            $this->logger->info("{$method} {$path}");
            if (!is_null($data)) {
                $this->logger->info(json_encode($data, JSON_THROW_ON_ERROR));
            }
            $this->logger->info($body);
        }

        $result = json_decode($body, true);

        if (is_scalar($result)) {
            if (is_numeric($result)) {
                $result = intval($result);
            }
            if ($result === 'true') {
                $result = true;
            }
            if ($result === 'false') {
                $resdult = false;
            }
            $result = ['result' => $result];
        }

        return recursiveCollect($result);
    }
}
