<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2;

/**
 * Encapsulates data for an update to a resource.
 */
class Update
{
    /** @var array<string, mixed> */
    protected array $request;

    public function __construct(
        private readonly int|string $key,
        private readonly Repository $repository,
    ) {
        $this->request = [];
    }

    /**
     * Run the update.
     */
    public function run(): void
    {
        $this->repository->sendUpdate($this->key, $this->request);
    }
}
