<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SlyFoxCreative\Magento2\Exception\NotFoundException;
use SlyFoxCreative\Magento2\Exception\UpdateException;

/**
 * A repository of Resources of a given type.
 *
 * Used to find Resources and cache their data so later finds don't have to
 * hit the API again.
 */
abstract class Repository
{
    /** @var Collection<int|string, Collection<int|string, mixed>> */
    protected readonly Collection $cache;

    public function __construct(
        protected readonly Client $client,
        protected readonly string $class,
    ) {
        $this->cache = collect();
    }

    /**
     *  Get all resources.
     *
     * @return Collection<int|string, \SlyFoxCreative\Magento2\Resource>
     */
    public function all(): Collection
    {
        $resources = $this->resources();

        $resources->each(function ($resource) {
            $this->cache->put(strval($resource[$this->cacheKey()]), $resource);
        });

        return $resources->map(function ($resource) {
            return $this->find($resource[$this->cacheKey()]);
        });
    }

    /**
     * Return whether the resource with the given key exists.
     */
    public function exists(int|string $key): bool
    {
        try {
            $this->data($key);
        } catch (NotFoundException $e) {
            return false;
        }

        return true;
    }

    /**
     * Find a resource by its ID.
     *
     * The ID's type depends on the type of resource.
     * E.g., a Product's ID is a string, a Categegory's ID is an int.
     */
    abstract public function find(int|string $key): Resource;

    /**
     * Return an Update for the Resource with the given ID.
     */
    public function update(int|string $key): Update
    {
        throw new UpdateException($this->class);
    }

    /**
     * Get the raw collection of data for the given ID.
     *
     * @return Collection<int|string, mixed>
     */
    public function data(int|string $key): Collection
    {
        $key = strval($key);
        if (! $this->cache->has($key)) {
            $basePath = $this->basePath();
            $this->cache->put($key, $this->client->get("/{$basePath}/{$key}"));
        }

        return $this->cache[$key];
    }

    /**
     * Send a request to to the API to update the resource, and update the
     * cached data.
     *
     * @param  array<string, mixed>  $data
     */
    public function sendUpdate(int|string $key, array $data): void
    {
        $basePath = $this->basePath();
        $key = strval($key);
        $requestKey = Str::snake(class_basename($this->class));
        $response = $this->client->put("/{$basePath}/{$key}", [$requestKey => $data]);
        $this->cache->put($key, $response);
    }

    /**
     * Return the collection of data for all resources from the API.
     *
     * @return Collection<int|string, mixed>
     */
    protected function resources(): Collection
    {
        $basePath = $this->allPath();

        $response = $this->client->get("/{$basePath}", ['searchCriteria' => ['currentPage' => 1]]);

        return $response['items'];
    }

    /**
     * Return the base API path for the Repository.
     */
    protected function basePath(): string
    {
        return Str::plural(Str::camel(class_basename($this->class)));
    }

    /**
     * Return the API path for getting all resources.
     */
    protected function allPath(): string
    {
        return $this->basePath();
    }

    /**
     * Return the attribute to use for as they key when storing a Resource
     * in the Repository cache.
     */
    abstract protected function cacheKey(): string;
}
