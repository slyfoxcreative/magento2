<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Tests;

use Psr\Log\AbstractLogger;

use function SlyFoxCreative\Utilities\assert_callable;

/**
 * Used for testing purposes.
 *
 * It records all records and gives you access to them for verification.
 *
 * @method hasInfo(string $record)
 */
class TestLogger extends AbstractLogger
{
    /** @var array<int, array<string, mixed>> */
    public array $records = [];

    /** @var array<int|string, array<int, array<string, mixed>>> */
    public array $recordsByLevel = [];

    /** @param array<string, mixed> $args */
    public function __call(string $method, array $args): mixed
    {
        if (preg_match('/(.*)(Debug|Info|Notice|Warning|Error|Critical|Alert|Emergency)(.*)/', $method, $matches) > 0) {
            $genericMethod = $matches[1] . ($matches[3] !== 'Records' ? 'Record' : '') . $matches[3];
            $level = mb_strtolower($matches[2]);
            if (method_exists($this, $genericMethod)) {
                $args[] = $level;

                $callable = [$this, $genericMethod];
                assert_callable($callable);
                return call_user_func_array($callable, $args);
            }
        }

        throw new \BadMethodCallException('Call to undefined method ' . get_class($this) . '::' . $method . '()');
    }

    /** @param array<int|string, mixed> $context */
    public function log(mixed $level, string|\Stringable $message, array $context = []): void
    {
        $record = [
            'level' => $level,
            'message' => $message,
            'context' => $context,
        ];

        $this->recordsByLevel[$record['level']][] = $record;
        $this->records[] = $record;
    }

    public function hasRecords(string $level): bool
    {
        return isset($this->recordsByLevel[$level]);
    }

    public function hasRecord(mixed $record, string $level): bool
    {
        if (is_string($record)) {
            $record = ['message' => $record];
        }

        return $this->hasRecordThatPasses(function ($rec) use ($record) {
            if ($rec['message'] !== $record['message']) {
                return false;
            }
            if (isset($record['context']) && $rec['context'] !== $record['context']) {
                return false;
            }

            return true;
        }, $level);
    }

    public function hasRecordThatContains(string $message, string $level): bool
    {
        return $this->hasRecordThatPasses(function ($rec) use ($message) {
            return mb_strpos($rec['message'], $message) !== false;
        }, $level);
    }

    public function hasRecordThatMatches(string $regex, string $level): bool
    {
        return $this->hasRecordThatPasses(function ($rec) use ($regex) {
            return preg_match($regex, $rec['message']) > 0;
        }, $level);
    }

    public function hasRecordThatPasses(callable $predicate, string $level): bool
    {
        if (! isset($this->recordsByLevel[$level])) {
            return false;
        }
        foreach ($this->recordsByLevel[$level] as $i => $rec) {
            if (call_user_func($predicate, $rec, $i)) {
                return true;
            }
        }

        return false;
    }

    public function reset(): void
    {
        $this->records = [];
        $this->recordsByLevel = [];
    }
}
