<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Tests;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use SlyFoxCreative\Magento2\Product\Repository;
use SlyFoxCreative\Magento2\Product\Update;

class ProductUpdateTest extends TestCase
{
    public function testUpdatePrice(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->put('/rest/default/V1/products/TTN*HD2P-10000AC-D')
                ->withJson([
                    'product' => [
                        'price' => 10,
                    ],
                ])
                ->willRespond(new Response(200, [], $this->fixture('updated_product')))
            ;
        }

        $repository = new Repository($this->client);
        $update = new Update('TTN*HD2P-10000AC-D', $repository);

        $update->price(10)->run();

        $product = $repository->find('TTN*HD2P-10000AC-D');

        self::assertSame(10, $product->price);
    }

    public function testUpdateQuantity(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->put('/rest/default/V1/products/TTN*HD2P-10000AC-D')
                ->withJson([
                    'product' => [
                        'extension_attributes' => [
                            'stock_item' => [
                                'qty' => 20,
                                'is_in_stock' => true,
                            ],
                        ],
                    ],
                ])
                ->willRespond(new Response(200, [], $this->fixture('updated_product')))
            ;
        }

        $repository = new Repository($this->client);
        $update = new Update('TTN*HD2P-10000AC-D', $repository);

        $update->quantity(20)->run();

        $product = $repository->find('TTN*HD2P-10000AC-D');

        self::assertSame(20, $product->quantity);
    }

    public function testUpdateWithQuantityZero(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->put('/rest/default/V1/products/TTN*HD2P-10000AC-D')
                ->withJson([
                    'product' => [
                        'extension_attributes' => [
                            'stock_item' => [
                                'qty' => 0,
                                'is_in_stock' => false,
                            ],
                        ],
                    ],
                ])
                ->willRespond(new Response(200, [], $this->fixture('updated_product_out_of_stock')))
            ;
        }

        $repository = new Repository($this->client);
        $update = new Update('TTN*HD2P-10000AC-D', $repository);

        $update->quantity(0)->run();

        $product = $repository->find('TTN*HD2P-10000AC-D');

        self::assertSame(0, $product->quantity);
        self::assertFalse($product->isInStock);
    }

    public function testUpdateDiscount(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->put('/rest/default/V1/products/TTN*HD2P-10000AC-D')
                ->withJson([
                    'product' => [
                        'tier_prices' => [[
                            'customer_group_id' => 0,
                            'qty' => 1,
                            'extension_attributes' => [
                                'percentage_value' => 30.5,
                            ],
                        ]],
                    ],
                ])
                ->willRespond(new Response(200, [], $this->fixture('updated_product')))
            ;
        }

        $repository = new Repository($this->client);
        $update = new Update('TTN*HD2P-10000AC-D', $repository);

        $update->groupDiscount(0, 30.5)->run();

        $product = $repository->find('TTN*HD2P-10000AC-D');

        self::assertSame(30.5, $product->discountFor(0));
    }

    public function testUpdateGtin(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->put('/rest/default/V1/products/TTN*HD2P-10000AC-D')
                ->withJson([
                    'product' => [
                        'custom_attributes' => [[
                            'attribute_code' => 'gtin',
                            'value' => '12345',
                        ]],
                    ],
                ])
                ->willRespond(new Response(200, [], $this->fixture('updated_product')))
            ;
        }

        $repository = new Repository($this->client);
        $update = new Update('TTN*HD2P-10000AC-D', $repository);

        $update->gtin('12345')->run();

        $product = $repository->find('TTN*HD2P-10000AC-D');

        self::assertSame('12345', $product->gtin);
    }

    public function testUpdateSpecialPrice(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->put('/rest/default/V1/products/TTN*HD2P-10000AC-D')
                ->withJson([
                    'product' => [
                        'custom_attributes' => [
                            [
                                'attribute_code' => 'special_price',
                                'value' => 10,
                            ],
                            [
                                'attribute_code' => 'special_from_date',
                                'value' => '2020-01-01',
                            ],
                            [
                                'attribute_code' => 'special_to_date',
                                'value' => '2020-01-31',
                            ],
                        ],
                    ],
                ])
                ->willRespond(new Response(200, [], $this->fixture('updated_product')))
            ;
        }

        $repository = new Repository($this->client);
        $update = new Update('TTN*HD2P-10000AC-D', $repository);

        $update->specialPrice(10, Carbon::parse('2020-01-01'), Carbon::parse('2020-01-31'))->run();

        $product = $repository->find('TTN*HD2P-10000AC-D');

        self::assertSame(10.0, $product->specialPrice);
        self::assertEquals(Carbon::parse('2020-01-01'), $product->specialFromDate);
        self::assertEquals(Carbon::parse('2020-01-31'), $product->specialToDate);
    }
}
