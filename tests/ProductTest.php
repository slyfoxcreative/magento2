<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Tests;

use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Carbon;
use SlyFoxCreative\Magento2\Product\Product;
use SlyFoxCreative\Magento2\Product\Repository;

class ProductTest extends TestCase
{
    protected Product $product;

    protected function setUp(): void
    {
        parent::setUp();

        self::expect(
            $this->once(),
            '/rest/default/V1/products/TTN*HD2P-10000AC-D',
            new Response(200, [], $this->fixture('product_3')),
        );

        $repository = new Repository($this->client);
        $this->product = $repository->find('TTN*HD2P-10000AC-D');
    }

    public function testAttribute(): void
    {
        self::assertSame('TTN*HD2P-10000AC-D', $this->product->sku);
    }

    public function testIssetAttribute(): void
    {
        self::assertTrue(isset($this->product->sku));
    }

    public function testCustomAttribute(): void
    {
        self::assertSame('HD2P-10000AC-D', $this->product->titanManualSku);
    }

    public function testIssetCustomAttribute(): void
    {
        self::assertTrue(isset($this->product->titanManualSku));
    }

    public function testCreatedAt(): void
    {
        self::assertEquals(Carbon::parse('2014-03-24 19:41:07.0 UTC'), $this->product->createdAt);
    }

    public function testIssetCreatedAt(): void
    {
        self::assertTrue(isset($this->product->createdAt));
    }

    public function testUpdatedAt(): void
    {
        self::assertEquals(Carbon::parse('2019-04-12 17:44:05.0 UTC'), $this->product->updatedAt);
    }

    public function testIssetUpdatedAt(): void
    {
        self::assertTrue(isset($this->product->updatedAt));
    }

    public function testDiscountFor(): void
    {
        self::assertSame(0.0, $this->product->discountFor(0));
    }

    public function testQuantity(): void
    {
        self::assertSame(0, $this->product->quantity);
    }

    public function testDump(): void
    {
        self::assertSame(json_decode($this->fixture('product_3'), true), $this->product->dump());
    }
}
