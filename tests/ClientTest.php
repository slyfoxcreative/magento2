<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Tests;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Response;
use SlyFoxCreative\Magento2\Client;
use SlyFoxCreative\Magento2\Exception\BadRequestException;
use SlyFoxCreative\Magento2\Exception\NotAuthorizedException;
use SlyFoxCreative\Magento2\Exception\NotFoundException;

class ClientTest extends TestCase
{
    public function testGet(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/categories/3',
            new Response(200, [], $this->fixture('category_3')),
        );

        $response = $this->client->get('categories/3');

        self::assertSame('Fleet', $response['name']);
    }

    public function testPost(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->post('/rest/default/V1/order/11815/ship')
                ->withJson(['items' => [['order_item_id' => 15410, 'qty' => 1]]])
                ->willRespond(new Response(200, [], $this->fixture('shipment')))
            ;
        }

        $response = $this->client->post('order/11815/ship', [
            'items' => [
                ['order_item_id' => 15410, 'qty' => 1],
            ],
        ]);

        self::assertSame(9760, $response['result']);
    }

    public function testPut(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->put('/rest/default/V1/products/TTN*HD2P-10000AC-D')
                ->withJson(['product' => ['price' => 10]])
                ->willRespond(new Response(200, [], $this->fixture('updated_product')))
            ;
        }

        $response = $this->client->put('products/TTN*HD2P-10000AC-D', [
            'product' => [
                'price' => 10,
            ],
        ]);

        self::assertSame(10, $response['price']);
    }

    public function testDelete(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->delete('/rest/default/V1/products/TTN*HD2P-10000AC-D/group-prices/1/tiers/1')
                ->willRespond(new Response(200, [], 'true'))
            ;
        }

        $response = $this->client->delete('products/TTN*HD2P-10000AC-D/group-prices/1/tiers/1');

        self::assertTrue($response);
    }

    public function testGetQueryParameters(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->get('/rest/default/V1/products')
                ->withQuery([
                    'searchCriteria' => [
                        'currentPage' => 1,
                        'pageSize' => 1,
                    ],
                ])
                ->willRespond(new Response(200, [], $this->fixture('products')))
            ;
        }

        $response = $this->client->get('/products', [
            'searchCriteria' => [
                'currentPage' => 1,
                'pageSize' => 1,
            ],
        ]);

        self::assertEquals(collect(['TTN*HD2P-9000AFE']), $response['items']->pluck('sku'));
    }

    public function testNormalizesRequestPath(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/categories/3',
            new Response(200, [], $this->fixture('category_3')),
        );

        $response = $this->client->get('/categories/3');

        self::assertSame('Fleet', $response['name']);
    }

    public function testStoreCodeOption(): void
    {
        self::expect(
            $this->once(),
            '/rest/premier_default/V1/categories/3',
            new Response(200, [], $this->fixture('category_3')),
        );

        if ($_ENV['MOCK']) {
            $client = new Client(
                client: $this->guzzleClient,
                token: 'testtoken',
                code: 'premier_default',
            );
        } else {
            $client = CLient::make(
                uri: $_ENV['URI'],
                token: $_ENV['TOKEN'],
                code: 'premier_default',
            );
        }

        $response = $client->get('categories/3');

        self::assertSame('Fleet', $response['name']);
    }

    public function testBadRequest(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/categories/a',
            new Response(400, [], $this->fixture('category_bad_request')),
        );

        self::expectException(BadRequestException::class);
        self::expectExceptionMessage('The "a" value\'s type is invalid. The "int" type was expected. Verify and try again.');

        $this->client->get('categories/a');
    }

    public function testNotAuthorized(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/categories/3',
            new Response(401, [], $this->fixture('category_not_authorized')),
        );

        self::expectException(NotAuthorizedException::class);
        self::expectExceptionMessage("The consumer isn't authorized to access Magento_Catalog::categories");

        if ($_ENV['MOCK']) {
            $client = $this->client;
        } else {
            $guzzleClient = new GuzzleClient(['base_uri' => $_ENV['URI']]);
            $client = new Client($guzzleClient, 'testtoken');
        }

        $client->get('categories/3');
    }

    public function testNotFound(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/categories/10000',
            new Response(404, [], $this->fixture('category_not_found')),
        );

        self::expectException(NotFoundException::class);
        self::expectExceptionMessage('No such entity with id = 10000');

        $this->client->get('categories/10000');
    }

    public function testProducts(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/products/TTN*HD2P-10000AC-D',
            new Response(200, [], $this->fixture('product_3')),
        );

        $product = $this->client->products()->find('TTN*HD2P-10000AC-D');

        self::assertSame('TTN*HD2P-10000AC-D', $product->sku);
    }

    public function testCustomerGroups(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/customerGroups/0',
            new Response(200, [], $this->fixture('customer_group_0')),
        );

        $group = $this->client->customerGroups()->find(0);

        self::assertSame('NOT LOGGED IN', $group->code);
    }

    public function testGroupPrices(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->delete('/rest/default/V1/products/TTN*HD2P-10000AC-D/group-prices/1/tiers/1')
                ->willRespond(new Response(200, [], 'true'))
            ;
        }

        $response = $this->client->groupPrices('TTN*HD2P-10000AC-D', 1)->delete(1);

        self::assertTrue($response);
    }

    public function testLogger(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->put('/rest/default/V1/products/TTN*HD2P-10000AC-D')
                ->withJson(['product' => ['price' => 10]])
                ->willRespond(new Response(200, [], $this->fixture('updated_product')))
            ;
        }

        $response = $this->client->put('products/TTN*HD2P-10000AC-D', [
            'product' => [
                'price' => 10,
            ],
        ]);

        self::assertTrue($this->logger->hasInfo('PUT /rest/default/V1/products/TTN*HD2P-10000AC-D'));
        self::assertTrue($this->logger->hasInfo('{"product":{"price":10}}'));
        self::assertTrue($this->logger->hasInfo($this->fixture('updated_product')));
    }
}
