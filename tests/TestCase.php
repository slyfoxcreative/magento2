<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Tests;

use BlastCloud\Guzzler\UsesGuzzler;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\Rule\InvokedCount;
use PHPUnit\Framework\TestCase as BaseTestCase;
use SlyFoxCreative\Magento2\Client;

class TestCase extends BaseTestCase
{
    use UsesGuzzler;

    protected TestLogger $logger;

    protected GuzzleClient $guzzleClient;

    protected Client $client;

    protected function setUp(): void
    {
        $this->logger = new TestLogger();
        if ($_ENV['MOCK']) {
            $this->guzzleClient = $this->guzzler->getClient(['base_uri' => $_ENV['URI']]);
            $this->client = new Client(
                client: $this->guzzleClient,
                token: 'testtoken',
                logger: $this->logger,
            );
        } else {
            $this->client = Client::make(
                uri: $_ENV['URI'],
                token: $_ENV['TOKEN'],
                logger: $this->logger,
            );
        }
    }

    protected function fixture(string $file): string
    {
        $file = file_get_contents(__DIR__ . "/fixtures/{$file}.json");

        if ($file === false) {
            throw new \Exception("Failed to read fixture file {$file}");
        }

        return $file;
    }

    protected function expect(InvokedCount $count, string $uri, Response $response): void
    {
        if (! $_ENV['MOCK']) {
            return;
        }

        $this
            ->guzzler
            ->expects($count)
            ->get($uri)
            ->withHeaders([
                'Authorization' => 'Bearer testtoken',
                'Accept' => 'application/json',
            ])
            ->willRespond($response)
        ;
    }
}
