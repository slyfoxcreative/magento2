<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Tests;

use GuzzleHttp\Psr7\Response;
use SlyFoxCreative\Magento2\Product\Repository;

class ProductRepositoryTest extends TestCase
{
    protected Repository $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = new Repository($this->client);
    }

    public function testAll(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->get('/rest/default/V1/products')
                ->withQuery([
                    'searchCriteria' => [
                        'currentPage' => 1,
                    ],
                ])
                ->willRespond(new Response(200, [], $this->fixture('products')))
            ;
        }

        $products = $this->repository->all();

        self::assertNotNull($products->first());
        self::assertSame('TTN*HD2P-9000AFE', $products->first()->sku);
    }

    // Ensure that the expected API call occurs only once. Don't run it when
    // mocking is disabled because there is no expectation in that case.
    public function testAllCaching(): void
    {
        if (! $_ENV['MOCK']) {
            self::markTestSkipped('Test is useless unless client is mocked.');
        }

        $this
            ->guzzler
            ->expects($this->once())
            ->get('/rest/default/V1/products')
            ->withQuery([
                'searchCriteria' => [
                    'currentPage' => 1,
                ],
            ])
            ->willRespond(new Response(200, [], $this->fixture('products')))
        ;

        $products = $this->repository->all();
        $this->repository->find('TTN*HD2P-9000AFE');
    }

    public function testExistsWithExistentProduct(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/products/TTN*HD2P-10000AC-D',
            new Response(200, [], $this->fixture('product_3')),
        );

        self::assertTrue($this->repository->exists('TTN*HD2P-10000AC-D'));
    }

    public function testExistsWithNonExistentProduct(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/products/FAKE',
            new Response(404, [], $this->fixture('product_not_found')),
        );

        self::assertFalse($this->repository->exists('FAKE'));
    }

    public function testFind(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/products/TTN*HD2P-10000AC-D',
            new Response(200, [], $this->fixture('product_3')),
        );

        $product = $this->repository->find('TTN*HD2P-10000AC-D');

        self::assertSame('TTN*HD2P-10000AC-D', $product->sku);
    }

    // Ensure that the expected API call occurs only once. Don't run it when
    // mocking is disabled because there is no expectation in that case.
    public function testFindCaching(): void
    {
        if (! $_ENV['MOCK']) {
            self::markTestSkipped('Test is useless unless client is mocked.');
        }

        self::expect(
            $this->once(),
            '/rest/default/V1/products/TTN*HD2P-10000AC-D',
            new Response(200, [], $this->fixture('product_3')),
        );

        $this->repository->find('TTN*HD2P-10000AC-D')->dump();
        $this->repository->find('TTN*HD2P-10000AC-D');
    }
}
