<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Tests;

use GuzzleHttp\Psr7\Response;
use SlyFoxCreative\Magento2\GroupPrice\Repository;

class GroupPriceRepositoryTest extends TestCase
{
    public function testExistsWithExistentPrice(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->get('/rest/default/V1/products/TTN*HD2P-10000AC-D/group-prices/2/tiers')
                ->willRespond(new Response(200, [], $this->fixture('group_prices')))
            ;
        }

        $repository = new Repository($this->client, 'TTN*HD2P-10000AC-D', 2);

        self::assertTrue($repository->exists(1));
    }

    public function testExistsWithNonExistentPrice(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->get('/rest/default/V1/products/TTN*HD2P-10000AC-D/group-prices/2/tiers')
                ->willRespond(new Response(200, [], $this->fixture('group_prices')))
            ;
        }

        $repository = new Repository($this->client, 'TTN*HD2P-10000AC-D', 2);

        self::assertFalse($repository->exists(2));
    }

    public function testData(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->get('/rest/default/V1/products/TTN*HD2P-10000AC-D/group-prices/2/tiers')
                ->willRespond(new Response(200, [], $this->fixture('group_prices')))
            ;
        }

        $repository = new Repository($this->client, 'TTN*HD2P-10000AC-D', 2);

        $price = $repository->find(1);

        self::assertSame(37.5, $price->value);
    }

    public function testDelete(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->delete('/rest/default/V1/products/TTN*HD2P-10000AC-D/group-prices/1/tiers/1')
                ->willRespond(new Response(200, [], 'true'))
            ;
        }

        $repository = new Repository($this->client, 'TTN*HD2P-10000AC-D', 1);

        $response = $repository->delete(1);

        self::assertTrue($response);
    }
}
