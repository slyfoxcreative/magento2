<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento2\Tests;

use GuzzleHttp\Psr7\Response;
use SlyFoxCreative\Magento2\CustomerGroup\Repository;
use SlyFoxCreative\Magento2\Exception\UpdateException;

class CustomerGroupRepositoryTest extends TestCase
{
    public function testExistsWithExistentGroup(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/customerGroups/0',
            new Response(200, [], $this->fixture('customer_group_0')),
        );

        $repository = new Repository($this->client);

        self::assertTrue($repository->exists(0));
    }

    public function testExistsWithNonExistentGroup(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/customerGroups/1000000',
            new Response(404, [], $this->fixture('customer_group_not_found')),
        );

        $repository = new Repository($this->client);

        self::assertFalse($repository->exists(1000000));
    }

    public function testFind(): void
    {
        self::expect(
            $this->once(),
            '/rest/default/V1/customerGroups/0',
            new Response(200, [], $this->fixture('customer_group_0')),
        );

        $repository = new Repository($this->client);
        $group = $repository->find(0);

        self::assertSame('NOT LOGGED IN', $group->code);
    }

    public function testAll(): void
    {
        if ($_ENV['MOCK']) {
            $this
                ->guzzler
                ->expects($this->once())
                ->get('/rest/default/V1/customerGroups/search')
                ->withQuery([
                    'searchCriteria' => [
                        'currentPage' => 1,
                    ],
                ])
                ->willRespond(new Response(200, [], $this->fixture('customer_groups')))
            ;
        }

        $repository = new Repository($this->client);
        $groups = $repository->all();

        $expected = collect([
            'NOT LOGGED IN',
            'Regular Customer',
            'Wholesale',
            'Retailer',
            'Level C',
            'Level B',
            'Verifone Contract',
            'Mansfield Oil',
            'Marathon Contract',
            'Level A',
            'Level E',
            'Level D',
        ]);

        self::assertEquals($expected, $groups->pluck('code'));
    }

    public function testUpdate(): void
    {
        self::expectException(UpdateException::class);
        self::expectExceptionMessage('CustomerGroup does not support updates');

        $repository = new Repository($this->client);
        $repository->update(1);
    }
}
